//
//  AppDelegate.h
//  VMOBTest
//
//  Created by William Peroche on 19/05/14.
//  Copyright (c) 2014 Sush Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
