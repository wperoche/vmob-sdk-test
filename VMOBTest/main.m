//
//  main.m
//  VMOBTest
//
//  Created by William Peroche on 19/05/14.
//  Copyright (c) 2014 Sush Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
